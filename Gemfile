source 'https://rubygems.org'

gem 'rails', '3.2.18'
gem 'activeadmin'
gem "attr_encrypted", "~> 1.2.1"
gem 'by_star',            git: "git://github.com/radar/by_star"
gem 'cancan'
gem 'carrierwave'
gem 'cloudinary'
gem "cocoon"
gem 'concerned_with'
gem 'devise', '2.0.4'
gem 'friendly_id', '~> 4.0.7'
gem 'gmaps4rails'
gem 'geocoder', '~> 1.1.5'
gem 'haml'
gem 'has_scope'
gem 'inherited_resources'
gem 'jquery-rails', '2.1.4'
gem 'jquery-ui-rails'
gem 'json',     '>=1.7.7'
gem 'kaminari'
gem 'koala'
gem 'oauth'
gem 'omniauth',           git: 'https://github.com/intridea/omniauth.git'
gem 'omniauth-facebook',  git: 'https://github.com/mkdynamic/omniauth-facebook.git'
gem 'omniauth-twitter',   git: 'https://github.com/arunagw/omniauth-twitter.git'
gem "paperclip-dropbox", "~> 1.1.3"
gem 'rack-cors', require: 'rack/cors'
gem 'rails3-jquery-autocomplete'
gem 'rake'
gem 'rolify', git: 'https://github.com/EppO/rolify.git'
gem 'sanitize'
gem 'mysql2', '~> 0.3.16'
gem 'newrelic_rpm'
gem 'simple_form'
gem 'stamp'
gem 'state_machine'
gem 'twitter', '>= 2.2.3'
gem 'validates_email_format_of', git: 'git://github.com/alexdunae/validates_email_format_of.git'
gem 'zip'

group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'

  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  gem 'therubyracer', :platforms => :ruby

  gem 'uglifier', '>= 1.0.3'
end

group :development do
  gem 'better_errors'
  gem 'byebug'
  gem 'binding_of_caller'
  gem 'grb'
  gem 'heroku'
  gem 'rack-mini-profiler'
  gem 'rb-readline'
  gem 'sextant'
end

group :test, :development do
  gem 'capybara'
  gem 'factory_girl_rails'
  gem 'faker'
  gem 'fakeweb'
  gem 'haml-rails'
  gem 'json_expressions'
  gem 'rspec-rails'
  gem 'shoulda', require: false
  gem 'turn', '0.9.6', require: false
  gem 'sunspot_solr'
end

group :test do
  gem 'database_cleaner'
  gem 'letters'
  gem 'timecop'
end
