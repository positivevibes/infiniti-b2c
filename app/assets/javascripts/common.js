﻿$(document).ready(function($) {
    $('#mega-menu-tut').dcMegaMenu({
        rowItems: '4',
        speed: 'fast',
        effect: 'slide'
    });

    $('#slidingFeatures').jshowoff({
        effect: 'slideLeft',
        controlText: { play: 'Play', pause: 'Pause', previous: 'Prev', next: 'Next' },
        hoverPause: false
    });

    $('.pagenav').click(function() {
        var e2 = $("#followinfiniti");
        var p = e2.position();
        $('html, body').stop().animate({ scrollTop: p.top }, 600);
    });

    $('.jshowoff-slidelinks').hide();

    //HomePage

    $('#clients img:gt(0)').hide();
    setInterval(function() { $('#clients :first-child').fadeOut().next('img').fadeIn().end().appendTo('#clients'); }, 3000);

    function cgrill() {
        //$('.scontainer .flying-text').css({ opacity: 0 }); //set all text opacity to 0

        $('.scontainer .active-text').animate({left: '36%' }, 100, function() { type(); });
        var char1 = 0;
        var caption = "California Grill";

        function type() {

            $('.active-text').html(caption.substr(0, char1++));

            if (char1 < caption.length + 1)
                setTimeout(function() {
                    type()
                }, 70);
            else {
                char1 = 0;
            }

        }
    }


    $("#cgrill").click(function() {
        cgrill();
    });
});
