$(function() {

    //var tablecloths = "<div class='rows'><div class='left'><img src='../assets/products/thumbs/plains_table_thumb.png' /></div><div class='right'>Basics<br /><em>Traditional patterns</em></div><div style='clear: both'></div></div><div class='rows'><div class='left'><img src='../assets/products/thumbs/classics_table_thumb.png' /></div><div class='right'>Classics<br /><em>Restaurateur Dream</em></div><div style='clear: both'></div></div><div class='rows'><div class='left'><img src='../assets/products/thumbs/jacquard_table_thumb.png' /></div><div class='right'>Jacquards<br /><em>Engineered patterns</em></div><div style='clear: both'></div></div><div class='rows'><div class='left'><img src='../assets/products/thumbs/dzire_table_thumb.png' /></div><div class='right'>D'zire<br /> <em>Revolutionary range</em></div><div style='clear: both'></div></div>";
    //var napkins = "<div class='rows'><div class='left'><img src='../assets/products/thumbs/plains_thumb.png' /></div><div class='right'>Basics<br /><em>Traditional patterns</em></div><div style='clear: both'></div></div><div class='rows'><div class='left'><img src='../assets/products/thumbs/classics_thumb.png' /></div><div class='right'>Classics<br /><em>Restaurateur Dream</em></div><div style='clear: both'></div></div><div class='rows'><div class='left'><img src='../assets/products/thumbs/jacquard_thumb.png' /></div><div class='right'>Jacquards<br /><em>Engineered patterns</em></div><div style='clear: both'></div></div><div class='rows'><div class='left'><img src='../assets/products/thumbs/dzire_thumb.png' /></div><div class='right'>D'zire<br /> <em>Revolutionary range</em></div><div style='clear: both'></div></div>";
    var tablecloths_v = "Table Cloths ranges across our four specialized brands viz: Basics, Classics, Jacquards and D'zire.</em>";
    var napkins_v = "Napkins ranges across our four specialized brands viz: Basics, Classics, Jacquards and D'zire.</em>";
    var tablerunners_v = "Table runners ranges across two of our four specialized brands viz: Basics, and Jacquards.</em>";
    var aprons_v = "Aprons ranges across one of our four specialized brands viz: Basics.</em>";
    var handtowels_v = "Hand Towels ranges across one of our four specialized brands viz: Basics.</em>";
    var bathtowels_v = "Bath Towels<hr width='70%' align='left'/><em>Bath Towels ranges across two of our four specialized brands viz: Basics and D'zire.</em>";
    var bathrobes_v = "Bathrobes<hr width='70%' align='left'/><em>BathRobes ranges across one of our four specialized brands viz: Basics</em>";
    var bathmat_v = "Bath Mat<hr width='70%' align='left'/><em>Bath Mat ranges across one of our four specialized brands viz: Basics</em>";
    var showercurtains_v = "Shower Curtains<hr width='70%' align='left'/><em>Shower Curtains ranges across our four specialized brands viz: Basics, Classics, Jacquards and D'zire.</em>";
    var flatsheets_v = "Flat Sheets<hr width='70%' align='left'/><em>Flat Sheets ranges across our four specialized brands viz: Basics, Classics, Jacquards and D'zire.</em>";
    var fitsheets_v = "Fitted Sheets<hr width='70%' align='left'/><em>Fitted Sheets ranges across our four specialized brands viz: Basics, Classics, Jacquards and D'zire.</em>";
    var pillow_v = "Pillowcase/Shams<hr width='70%' align='left'/><em>Pillowcases/Shams ranges across our four specialized brands viz: Basics, Classics, Jacquards and D'zire.</em>";
    var duvet_v = "Duvet Covers<hr width='70%' align='left'/><em>Duvet Covers ranges across our four specialized brands viz: Basics, Classics, Jacquards and D'zire.</em>";
    var tablecloths_bind = "<div class='mp_content'><img src='../assets/products/main/tablecloth_basics.png' alt='img' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>Basics</h2><p>Basics come in traditional plain patterns and are multipurpose in their use. Ideal fit for coffee shops, 3-star category restaurants or outdoor catering events.</p></div></div><div class='mp_content' style='display: none;'><img src='../assets/products/main/tablecloth_classics.png' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>Classics</h2><p>Classics come in Satin Band or Patridge Eye/Bird’s Eye View patterns. Ideal fit for 3-star and 4-star category restaurants and catering events.</p></div></div><div class='mp_content' style='display: none;'><img src='../assets/products/main/tablecloth_jacquards.png' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>Jacquards</h2><p>Jacquards come in All Over or Engineered patterns. Ideal fit for 5 star category restaurant and catering events.</p></div></div><div class='mp_content' style='display: none;'><img src='../assets/products/main/tablecloth_dzire.png' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>D'Zire</h2><p>D’zire collection is a revolutionary range of Table Linen with customized woven logos, designs and themes. Pioneering approach of personalizing linens for elite restaurants, hotels and banquets. It is the ultimate in upscale napery with softness and elegance compared to none.</p></div></div>";
    var aprons_bind = "<div class='mp_content'><img src='../assets/products/main/aprons.png' alt='img' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>Basics</h2><p>Basics come in traditional plain patterns and are multipurpose in their use. Ideal fit for coffee shops, 3-star category restaurants or outdoor catering events.</p></div></div><div class='mp_content' style='display: none;'><img src='../assets/products/main/tablecloth_classics.png' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>Classics</h2><p>Classics come in Satin Band or Patridge Eye/Bird’s Eye View patterns. Ideal fit for 3-star and 4-star category restaurants and catering events.</p></div></div><div class='mp_content' style='display: none;'><img src='../assets/products/main/tablecloth_jacquards.png' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>Jacquards</h2><p>Jacquards come in All Over or Engineered patterns. Ideal fit for 5 star category restaurant and catering events.</p></div></div><div class='mp_content' style='display: none;'><img src='../assets/products/main/tablecloth_dzire.png' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>D'Zire</h2><p>D’zire collection is a revolutionary range of Table Linen with customized woven logos, designs and themes. Pioneering approach of personalizing linens for elite restaurants, hotels and banquets. It is the ultimate in upscale napery with softness and elegance compared to none.</p></div></div>";
    var handtowels_bind = "<div class='mp_content'><img src='../assets/products/main/handtowels.png' alt='img' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>Basics</h2><p>Basics come in traditional plain patterns and are multipurpose in their use. Ideal fit for coffee shops, 3-star category restaurants or outdoor catering events.</p></div></div><div class='mp_content' style='display: none;'><img src='../assets/products/main/tablecloth_classics.png' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>Classics</h2><p>Classics come in Satin Band or Patridge Eye/Bird’s Eye View patterns. Ideal fit for 3-star and 4-star category restaurants and catering events.</p></div></div><div class='mp_content' style='display: none;'><img src='../assets/products/main/tablecloth_jacquards.png' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>Jacquards</h2><p>Jacquards come in All Over or Engineered patterns. Ideal fit for 5 star category restaurant and catering events.</p></div></div><div class='mp_content' style='display: none;'><img src='../assets/products/main/tablecloth_dzire.png' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>D'Zire</h2><p>D’zire collection is a revolutionary range of Table Linen with customized woven logos, designs and themes. Pioneering approach of personalizing linens for elite restaurants, hotels and banquets. It is the ultimate in upscale napery with softness and elegance compared to none.</p></div></div>";
    var trunners_bind = "<div class='mp_content'><img src='../assets/products/main/trunners.png' alt='img' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>Basics</h2><p>Basics come in traditional plain patterns and are multipurpose in their use. Ideal fit for coffee shops, 3-star category restaurants or outdoor catering events.</p></div></div><div class='mp_content' style='display: none;'><img src='../assets/products/main/tablecloth_classics.png' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>Classics</h2><p>Classics come in Satin Band or Patridge Eye/Bird’s Eye View patterns. Ideal fit for 3-star and 4-star category restaurants and catering events.</p></div></div><div class='mp_content' style='display: none;'><img src='../assets/products/main/trunners_2.png' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>Jacquards</h2><p>Jacquards come in All Over or Engineered patterns. Ideal fit for 5 star category restaurant and catering events.</p></div></div><div class='mp_content' style='display: none;'><img src='../assets/products/main/tablecloth_dzire.png' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>D'Zire</h2><p>D’zire collection is a revolutionary range of Table Linen with customized woven logos, designs and themes. Pioneering approach of personalizing linens for elite restaurants, hotels and banquets. It is the ultimate in upscale napery with softness and elegance compared to none.</p></div></div>";
    var napkin_bind = "<div class='mp_content'><img src='../assets/products/main/plains.png' alt='img' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>Basics</h2><p>Basics come in traditional plain patterns and are multipurpose in their use. Idealfit can range from a coffee shop to a 3-star restaurant or an outdoor catering eventto a Banquet hall.</p></div></div><div class='mp_content' style='display: none;'><img src='../assets/products/main/classics.png' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>Classics</h2><p>Classics come in Satin Band or Patridge Eye/Bird’s Eye View patterns. Ideal fitfor 3-star and 4-star category restaurant.</p></div></div><div class='mp_content' style='display: none;'><img src='../assets/products/main/jacquard.png' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>Jacquards</h2><p>Jacquards come in All Over or Engineered patterns. Ideal fit ranges from 5 starcategory restaurants to high end banquet halls.</p></div></div><div class='mp_content' style='display: none;'><img src='../assets/products/main/dzire.png' /><a href='#' class='mp_playall'>Play all</a><div class='mp_description'><h2>D'Zire</h2><p>D’zire collection is a revolutionary range of table linen with woven logos, designs and themes. A pioneering approach to personalizing Table Linen for those very specialoccasions.</p></div></div>";
    $('#mp_albums').jcarousel({
        scroll: 3
    });

    function curtains() {
        var curtainheight = $('.backgroundeffect').height();
        $('.backgroundeffect').remove();
        //while (curtainheight > 0) {
        //curtainheight -= 10;
        //$('.backgroundeffect').delay(5000).css({ 'min-height': curtainheight + 'px' }).fadeIn(3000);

        // }

    }

    var yw = "default";
    var url = self.location.href;
    var query = url.lastIndexOf('?') + 1;
    var querystring = url.substring(query);
    defaultlinen(querystring);

    function defaultlinen(yw) {

        $('#mp_albums img').css({ 'border': 'solid 0px #fff', 'opacity': '0.5' });

        if (yw == "tablecloths") {
            curtains();
            //$('.rowbar').fadeOut().html(tablecloths).fadeIn("slow");
            $('.detailedp').html(tablecloths_v);
            $('#mp_content_wrapper').html(tablecloths_bind);
            $(".rows").css({ 'background-image': 'url(../assets/rows.png)' });
            $(".headertable").html("Table Linen - Table Cloths");
            $("#" + yw).addClass('currentselected');
            $("#" + yw).find('.subtitle').css({ 'color': '#DABA37' });
            $("#" + yw).find('img').css({ 'border': 'solid 2px #fff', 'opacity': '1' });
        }
        if (yw == "napkins") {
            curtains();
            //$('.rowbar').fadeOut().html(napkins).fadeIn("slow");
            $('.detailedp').html(napkins_v);
            $('#mp_content_wrapper').html(napkin_bind);
            $(".rows").css({ 'background-image': 'url(../assets/rows.png)' });
            $(".headertable").html("Table Linen - Napkins");
            $("#" + yw).addClass('currentselected');
            $("#" + yw).find('.subtitle').css({ 'color': '#DABA37' });
            $("#" + yw).find('img').css({ 'border': 'solid 2px #fff', 'opacity': '1' });
        }
        if (yw == "trunners") {
            curtains();
            $('.detailedp').html(tablerunners_v);
            $('#mp_content_wrapper').html(trunners_bind);
            $(".rows").css({ 'background-image': 'url(../assets/rows.png)' });
            $(".headertable").html("Table Linen - Table Runners");
            $(".rows:contains('Classics')").css({ 'background-image': 'url(../assets/pattlock.png)' }).append("<img src='../assets/cross.png' class='lockimage_1'>");
            $(".rows:contains('D'zire')").css({ 'background-image': 'url(../assets/pattlock.png)' }).append("<img src='../assets/cross.png' class='lockimage_3'>");
            $("#" + yw).addClass('currentselected');
            $("#" + yw).find('.subtitle').css({ 'color': '#DABA37' });
            $("#" + yw).find('img').css({ 'border': 'solid 2px #fff', 'opacity': '1' });
        }

        if (yw == "aprons") {
            curtains();
            $('.detailedp').html(aprons_v);
            $('#mp_content_wrapper').html(aprons_bind);
            $(".headertable").html("Table Linen - Aprons");
            $(".rows:contains('Classics')").css({ 'background-image': 'url(../assets/pattlock.png)' }).append("<img src='../assets/cross.png' class='lockimage_1'>");
            $(".rows:contains('D'zire')").css({ 'background-image': 'url(../assets/pattlock.png)' }).append("<img src='../assets/cross.png' class='lockimage_3'>");
            $(".rows:contains('Jacquards')").css({ 'background-image': 'url(../assets/pattlock.png)' }).append("<img src='../assets/cross.png' class='lockimage_2'>");
            $("#" + yw).addClass('currentselected');
            $("#" + yw).find('.subtitle').css({ 'color': '#DABA37' });
            $("#" + yw).find('img').css({ 'border': 'solid 2px #fff', 'opacity': '1' });
        }
        if (yw == "handtowels") {
            curtains();
            $('.detailedp').html(handtowels_v);
            $('#mp_content_wrapper').html(handtowels_bind);
            $(".headertable").html("Table Linen - HandTowels");
            $(".rows:contains('Classics')").css({ 'background-image': 'url(../assets/pattlock.png)' }).append("<img src='../assets/cross.png' class='lockimage_1'>");
            $(".rows:contains('D'zire')").css({ 'background-image': 'url(../assets/pattlock.png)' }).append("<img src='../assets/cross.png' class='lockimage_3'>");
            $(".rows:contains('Jacquards')").css({ 'background-image': 'url(../assets/pattlock.png)' }).append("<img src='../assets/cross.png' class='lockimage_2'>");
            $("#" + yw).addClass('currentselected');
            $("#" + yw).find('.subtitle').css({ 'color': '#DABA37' });
            $("#" + yw).find('img').css({ 'border': 'solid 2px #fff', 'opacity': '1' });
        }


    }

    $('#mp_albums li').live('click', function() {
        curtains();
        //$('.backgroundeffect').remove();
        //when clicking on an album, display its info, and hide the current one
        if ($(this).attr('class').indexOf("currentselected") != -1) {
        }
        else {
            $('.lockimage').remove();
            $('.lockimage_1').remove();
            $('.lockimage_2').remove();
            $('.lockimage_3').remove();
            var x = $(this).attr("id");
            if (x == "tablecloths") {
                //$('.rowbar').fadeOut().html(tablecloths).fadeIn("slow");
                $('.detailedp').html(tablecloths_v);
                $('#mp_content_wrapper').html(tablecloths_bind);
                $(".rows").css({ 'background-image': 'url(../assets/rows.png)' });
                $(".headertable").html("Table Linen - Table Cloths");
            }
            if (x == "napkins") {
                //$('.rowbar').fadeOut().html(napkins).fadeIn("slow");
                $('.detailedp').html(napkins_v);
                $('#mp_content_wrapper').html(napkin_bind);
                $(".rows").css({ 'background-image': 'url(../assets/rows.png)' });
                $(".headertable").html("Table Linen - Napkins");
            }
            if (x == "trunners") {
                $('.detailedp').html(tablerunners_v);
                $('#mp_content_wrapper').html(trunners_bind);
                $(".rows").css({ 'background-image': 'url(../assets/rows.png)' });
                $(".headertable").html("Table Linen - Table Runners");
                $(".rows:contains('Classics')").css({ 'background-image': 'url(../assets/pattlock.png)' }).append("<img src='../assets/cross.png' class='lockimage_1'>");
                $(".rows:contains('D'zire')").css({ 'background-image': 'url(../assets/pattlock.png)' }).append("<img src='../assets/cross.png' class='lockimage_3'>");
            }

            if (x == "aprons") {
                $('.detailedp').html(aprons_v);
                $('#mp_content_wrapper').html(aprons_bind);
                $(".headertable").html("Table Linen - Aprons");
                $(".rows:contains('Classics')").css({ 'background-image': 'url(../assets/pattlock.png)' }).append("<img src='../assets/cross.png' class='lockimage_1'>");
                $(".rows:contains('D'zire')").css({ 'background-image': 'url(../assets/pattlock.png)' }).append("<img src='../assets/cross.png' class='lockimage_3'>");
                $(".rows:contains('Jacquards')").css({ 'background-image': 'url(../assets/pattlock.png)' }).append("<img src='../assets/cross.png' class='lockimage_2'>");
            }
            if (x == "handtowels") {
                $('.detailedp').html(handtowels_v);
                $('#mp_content_wrapper').html(handtowels_bind);
                $(".headertable").html("Table Linen - HandTowels");
                $(".rows:contains('Classics')").css({ 'background-image': 'url(../assets/pattlock.png)' }).append("<img src='../assets/cross.png' class='lockimage_1'>");
                $(".rows:contains('D'zire')").css({ 'background-image': 'url(../assets/pattlock.png)' }).append("<img src='../assets/cross.png' class='lockimage_3'>");
                $(".rows:contains('Jacquards')").css({ 'background-image': 'url(../assets/pattlock.png)' }).append("<img src='../assets/cross.png' class='lockimage_2'>");
            }

            $('#mp_albums li').each(function() {
                $(this).find('.subtitle').css({ 'color': '#fff' });
                $(this).removeClass('currentselected');
            });

            $('#mp_albums img').css({ 'border': 'solid 0px #fff', 'opacity': '0.5' });
            $(this).addClass('currentselected');
            $(this).find('.subtitle').css({ 'color': '#DABA37' });
            $(this).find('img').css({ 'border': 'solid 2px #fff', 'opacity': '1' });

            $('.detailed').fadeOut().fadeIn('100');
        }


    });

    $('#mycarousel img').hover(
                    function() {
                        $(this).parent().find('span').stop().animate({ 'left': '0px' }, 300);
                    },
                   function() {
                       $(this).parent().find('span').stop().animate({ 'left': '65px' }, 300);
                   });



    $('.mp_player div.rows').live('click', function() {

        var $this = $(this);
        var retid = $('.currentselected').attr('id');
        var cbrand = $(this).index();

        if (retid == "trunners" && (cbrand == 1 || cbrand == 3)) {
            $('.detailedp').html('Sorry, this brand is not available for this item');
            $('.detailed').fadeOut().fadeIn('100');
            return false;
        }
        if (retid == "trunners" && (cbrand == 0 || cbrand == 2)) {
            $('.detailedp').html(tablerunners_v);
        }

        if ((retid == "aprons" || retid == "handtowels") && (cbrand == 1 || cbrand == 3 || cbrand == 2)) {
            $('.detailedp').html('Sorry, this brand is not available for this item');
            $('.detailed').fadeOut().fadeIn('100');
            return false;
        }

        if (retid == "aprons" && (cbrand == 0)) {
            $('.detailedp').html(aprons_v);
        }

        if (retid == "handtowels" && (cbrand == 0)) {
            $('.detailedp').html(handtowels_v);
        }


        $('#mp_content_wrapper').find('.mp_content:visible')
                                            .hide();

        $('#mp_content_wrapper').find('.mp_content:nth-child(' + parseInt($this.index() + 1) + ')')
                                                .fadeIn(1000);

        $('.detailed').fadeOut().fadeIn('100');


    });


    $('#mega-menu-tut').dcMegaMenu({
        rowItems: '4',
        speed: 'fast'
    });

    $('#mycarousel').jcarousel({
        vertical: true,
        scroll: 4
    });

    $('.imgOpa').each(function() {
        $(this).hover(
                    function() {
                        $(this).stop().animate({ opacity: 1.0 }, 800);
                    },
                   function() {
                       $(this).stop().animate({ opacity: 0.3 }, 800);
                   })
    });
    var i = 0;
    $('.closedesc').click(function() {
        if (i == 0) {
            $(this).css({ 'top': '88.8%' });
            $(this).html('Show');
            $(this).nextAll().animate({ top: '532px' }, 800, function() { $(this).hide(); });
            i = 1;
        } else {
            $(this).html('Hide');
            $(this).nextAll().show().animate({ top: '290px' }, 800, function() { $(this).prev().css({ 'top': '62%' }); $(this).show(); });
            i = 0;
        }
    });

    $("#colorshade").fancybox({
        'width': '400px',
        'height': '120%',
        'titlePosition': 'inside',
        'transitionIn': 'none',
        'transitionOut': 'none'
    });

    $("#qualitychart").fancybox({
        'width': '400px',
        'height': '120%',
        'titlePosition': 'inside',
        'transitionIn': 'none',
        'transitionOut': 'none'
    });
});
