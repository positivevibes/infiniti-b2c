$(document).ready(function() {
    $('.messagewrapper input').keypress(function(e) {
        $('.errorstar').remove();
        $('#notifications').html("").hide();
    });

    $('.commentForm').live('ajax:beforeSend', function(){
        checkFields();
    })

    $('.send_newsletter').live('ajax:beforeSend', function(){
        var check = check_news_letter();
        return check;
    })

    function check_news_letter(){
        var ice = true;
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

        $(".send_newsletter input[type='text']").each(function() {
            if ($(this).val() == "" || $(this).val() == undefined) {
                alert("Please enter your email.");
                ice = false;
            }
        });

        if (!emailReg.test($('.newsletterEmail').val())) {
            alert("Please enter a valid email id");
            ice = false;
        }

        return ice;
    }

    function checkFields(){
        var ice = true;
        $('#notifications').html("");
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

        $('.messagewrapper input').each(function() {
            if ($(this).val() == "" || $(this).val() == undefined) {
                $("<strong class='errorstar' style='color:red;position:absolute;'>*</strong>").insertAfter(this);
                $('#notifications').html("Please fill all the required fields").show();
                ice = false;
            }
        });

        if (!emailReg.test($('#contactEmail').val())) {
            $('#notifications').html("Please enter a valid email id").show();
            ice = false;
        }
        return ice;
    }
});
