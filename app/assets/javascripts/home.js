﻿$(function() {
    $("#cgrill").fancybox({
        'width': '400px',
        'height': '120%',
        'titlePosition': 'inside',
        'transitionIn': 'none',
        'transitionOut': 'none'
    });

    $("#cshade").fancybox({
        'width': '400px',
        'height': '120%',
        'titlePosition': 'inside',
        'transitionIn': 'none',
        'transitionOut': 'none'
    });

    $("#designc").fancybox({
        'width': '400px',
        'height': '120%',
        'titlePosition': 'inside',
        'transitionIn': 'none',
        'transitionOut': 'none'
    });
});
