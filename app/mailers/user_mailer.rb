class UserMailer < ActionMailer::Base
  default :from => "info@infinitilinens.com"

  def send_contact_email(name, email, subject, message)
    @name = name
    @email = email
    @message = message
    mail(
      reply_to: email,
      to:       DEVELOPMENT_EMAIL,
      subject:  subject)
  end

  def send_newsletter_email(from)
    @from    = from
    subject  = 'A new user has subscribed to our newsletter'
    mail(reply_to: from, to: DEVELOPMENT_EMAIL, subject: subject)
  end
end
