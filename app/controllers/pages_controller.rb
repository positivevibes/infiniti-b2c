class PagesController < ApplicationController
  def home
  end

  def about
  end

  def contact
  end

  def portfolio
  end

  def products
  end

  def privacy
  end

  def sitemap
  end

  # Email related actions
  def send_contact_email
    UserMailer.send_contact_email(params[:name], params[:email],
        params[:subject], params[:message]).deliver
    head :ok
  end

  def send_newsletter_email
    UserMailer.send_contact_email(params[:email]).deliver
    head :ok
  end
end
