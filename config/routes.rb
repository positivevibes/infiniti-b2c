Infiniti::Application.routes.draw do
  root :to => 'pages#home'

  resources :pages do
    collection do
      post :send_contact_email
      post :send_newsletter_email
    end
  end

  match "home", to: "pages#home", as: :home
  match "about", to: "pages#about", as: :about
  match "contact", to: "pages#contact", as: :contact
  match "portfolio", to: "pages#portfolio", as: :portfolio
  match "products", to: "pages#products", as: :products
  match "privacy_policy", to: "pages#privacy", as: :privacy
  match "sitemap", to: "pages#sitemap", as: :sitemap
end
